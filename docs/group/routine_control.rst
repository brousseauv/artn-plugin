.. _group_routine_control:

Routine Control
===============

.. #doxygengroup:: Control
   :project: plugin-ARTn

:ref:`f90_nextmin`
------------------
Option can be use with ``lmove_nextmin = .true.``

:ref:`f90_push_init` 
--------------------
Custom the initial push in basin

:ref:`f90_read_guess`
---------------------
Allow to read the initial push and/or the initial eigenvector through a file 

:ref:`f90_smooth_interpol`
--------------------------
Custom the transition betwen the initial push and the eigenvector direction by an interpolation between them



